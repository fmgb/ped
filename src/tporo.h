/* 
 * File:   tporo.h
 * Author: Francisco Manuel García Botella, 48570019-E
 *
 * Created on 30 de enero de 2013, 16:22
 */

#ifndef TPORO_H
#define	TPORO_H
#include <iostream>
#include <iomanip>
#include <string.h>


using namespace std;

      static string ErrorReserva = "Error: Ha fallado la reserva de memoria.";
    
class TPoro {
    
    // Sobrecarga del operador SALIDA
    friend ostream & operator<<(ostream &,const  TPoro &);
    

        
public:
    //Constructor por defecto.
    TPoro();
    //Constructor de copia.
    TPoro(const TPoro&) ;
    //Constructor a partir de una posición y un volumen.
    TPoro(int, int, double);
    //Constructor a partir de una posición, un volumen y un color.
    TPoro(int, int, double, char *);
    // Sobrecarga del operador asignación
    TPoro & operator=(TPoro &);
    // Destructor
    ~TPoro();

    // Sobrecarga del operador igualdad
    bool operator==(TPoro & poro);
    // Sobrecarga del operador desigualdad
    bool operator!=(TPoro & poro) { return (!this->operator ==(poro));} //PROBARLO
    // Modifica la posición
    void Posicion(int, int);
    // Modifica el volumen
    void Volumen(double volumen) { this->volumen = volumen;} //POSIBLE MODIFICACION
    // Modifica el color
    void Color(char *color);
    // Devuelve la coordenada x de la posición
    int PosicionX() const { return this->x; }
    // Devuelve la coordenada y de la posición
    int PosicionY() const{ return this->y; }
    // Devuelve el volumen
    double Volumen() const { return this->volumen; }
    // Devuelve el color
    char * Color() const { return this->color; }
    // Devuelve cierto si el poro está vacío
    bool EsVacio() const;

private:
    //Coordenada x de la posición.
    int x;
    //Coordenada y de la posición.
    int y;
    //Volumen.
    double volumen;
    // Color.
    char* color;
    

    //Método auxiliar de copia.
   void Copia(int x, int y, double volumen, char * color);
   
   char * ColorMinusculas(char * color);
};

#endif	/* TPORO_H */

