/* 
 * File:   tlistaporo.cpp
 * Author: fmgb92
 * 
 * Created on 10 de marzo de 2013, 20:56
 */

#include "../include/tlistaporo.h"
//#include <iostream> //DUDA PORQUé SI QUITO ESTO SE ME quitan el color de los NULL.

/*             _____        ____       _____
 |||\   ||| |||_____|||  |||____\   |||_____|||
 |||\\  ||| |||     |||  |||    \\  |||     |||
 |||\\\ ||| |||     |||  |||     )) |||     |||
 ||| \\\||| |||_____|||  |||____//  |||_____|||
 |||  \\||| |||_____|||  |||____/   |||_____|||

 */
TListaNodo::TListaNodo() : e() {
	this->anterior = this->siguiente = NULL;
}

//Constructor de copia

TListaNodo::TListaNodo(const TListaNodo & orig) : e(orig.e) {
	this->anterior = NULL;
	this->siguiente = NULL;
}

//Destructor

TListaNodo::~TListaNodo() {
	this->anterior = this->siguiente = NULL;
}

//Sobrecarga del operador asignación

TListaNodo&
TListaNodo::operator=(TListaNodo & orig) {
	if (this != &orig) {
		(*this).~TListaNodo();
		//	cout<<"HOLA" <<endl;
		this->e = orig.e;
		this->anterior = orig.anterior;
		this->siguiente = orig.siguiente;
	}
	return (*this);
}


/*                                _                _
                                 |_|              |_|           ///
    __       _____       ____            _______               ///
 |||__\   |||_____|||   /____    |||  |||_______  |||      _____      |||\   |||
 |||  \|  |||     |||  ((___     |||  |||         |||   |||_____|||   |||\\  |||
 |||__/|  |||     |||  \____\    |||  |||         |||   |||     |||   |||\\\ |||
 |||__/   |||     |||       \\   |||  |||         |||   |||     |||   ||| \\\|||
 |||      |||_____|||   ____//   |||  |||_______  |||   |||_____|||   |||  \\|||
 |||      |||_____|||   ____/    |||  |||_______  |||   |||_____|||   |||   \|||
 */

//Constructor por defecto

TListaPosicion::TListaPosicion() {
	this->pos = NULL;
}

//Constructor de copia

TListaPosicion::TListaPosicion(const TListaPosicion & orig) {
	this->pos = orig.pos;
}

//Destructor

TListaPosicion::~TListaPosicion() {
	pos = NULL;
}

//Sobrecarga del operador asignación

TListaPosicion & TListaPosicion::operator=(const TListaPosicion & orig) {
	if(this != &orig) {
		(*this).~TListaPosicion();
		this->pos = orig.pos;
	}
	return (*this);
}

//Sobrecarga del operador igualdad

bool TListaPosicion::operator==(const TListaPosicion & orig) {
	if(this->pos == orig.pos) {
		return true;
	}
	else {
		return false;
	}
}

//Devuelve TRUE si la posición no apunta a una lista, FALSE en caso contario.

bool TListaPosicion::EsVacia() {
	if(this->pos == NULL)
		return true;
	else
		return false;
}

//Devuelve la posicion anterior
TListaPosicion  TListaPosicion::Anterior() { //PROBAR CUIDADO CON LA &
	TListaPosicion aux;
	if(this->pos->anterior != NULL) {
		aux.pos = this->pos->anterior;
	}
	return aux;
}

//Devuelve la posicion siguiente.
TListaPosicion TListaPosicion::Siguiente() { //PROBAR CUIDADO CON EL &
	TListaPosicion aux;
	if(this->pos->siguiente != NULL) {
		aux.pos = pos->siguiente;
	}
	return aux;
}

/*  __      _____       __      _____
 |||__\  |||_____||| |||__\  |||_____|||
 |||  \| |||     ||| |||  \| |||     |||
 |||__/| |||     ||| |||__/| |||     |||
 |||__/  |||     ||| |||__/  |||     |||
 |||     |||_____||| |||\\\  |||_____|||
 |||     |||_____||| ||| \\\ |||_____|||

 */

ostream & operator<<(ostream & salida, const TListaPoro & lista) {
	salida<<"(";
	if(!lista.EsVacia()) {
		TListaPosicion aux;
		aux = lista.Primera();
		while(!aux.EsVacia()) {

			salida<<lista.Obtener(aux);
			if(!aux.Siguiente().EsVacia()) {
				salida<<" ";
			}

			// 		cout<<endl <<aux.Siguiente() <<endl;
			aux = aux.Siguiente();

		}
	}
	salida<<")";

	return salida;
}
//Constructor por defecto.

TListaPoro::TListaPoro() { //COMPROBAR
	primero = NULL;
	ultimo = NULL;
	//numelem = 0;
}

//Constructor de copia.

TListaPoro::TListaPoro(const TListaPoro & orig) { //COMPROBAR
	if(!orig.EsVacia()) {
		TListaPosicion recorre, aux1, aux2;
		recorre = orig.Primera();
		primero = new TListaNodo(*recorre.pos);
		aux1 = this->Primera();
		recorre = recorre.Siguiente();
		while (!recorre.EsVacia()) {
			aux2 = aux1;
			aux1 = aux1.Siguiente();
			aux1.pos = new TListaNodo(*recorre.pos);
			aux1.pos->anterior = aux2.pos;
			aux2.pos->siguiente = aux1.pos;
			recorre = recorre.Siguiente();
		}
		this->ultimo = aux1.pos;
	}
}

//Destructor.

TListaPoro::~TListaPoro() { //COMPROBAR
	TListaPosicion aux1,aux2;
	aux1 = this->Primera();
	while(!aux1.EsVacia()) {
		aux2 = aux1;
		aux1 = aux1.Siguiente();
		delete aux2.pos;
	}
	primero = ultimo = NULL;
}

// Sobrecarga del operador asignación

TListaPoro & TListaPoro::operator=(const TListaPoro & orig) {
	(*this).~TListaPoro();
	TListaPosicion recorre, aux1, aux2;
	recorre = orig.Primera();
	primero = new TListaNodo(*recorre.pos);
	aux1 = this->Primera();
	recorre = recorre.Siguiente();
	while (!recorre.EsVacia()) {
		aux2 = aux1;
		aux1 = aux1.Siguiente();
		aux1.pos = new TListaNodo(*recorre.pos);
		aux1.pos->anterior = aux2.pos;
		aux2.pos->siguiente = aux1.pos;
		recorre = recorre.Siguiente();
	}
	this->ultimo = aux1.pos;
	return (*this);
}


//MÉTODOS

// Sobrecarga del operador igualdad

bool TListaPoro::operator==(TListaPoro & orig) {
	if(this->Longitud() == orig.Longitud()) {
		TListaPosicion aux,lista;
		aux = this->Primera();
		lista = orig.Primera();
		for(int i = 0; i < this->Longitud();i++) {
			if(lista.pos != aux.pos) return false;
			aux = aux.Siguiente();
			lista = lista.Siguiente();
		}
		return true;
	}
	else {
		return false;
	}
}

// Sobrecarga del operador suma

TListaPoro TListaPoro::operator+(TListaPoro & lista) {
	TListaPoro l;
	TListaPosicion recorre,recorrelista;
	recorre = lista.Primera();
	while(!recorre.EsVacia()) {
		l.Insertar(recorre.pos->e);
		recorre = recorre.Siguiente();
	}
	recorre = this->Primera();
	while(!recorre.EsVacia()) {
		l.Insertar(recorre.pos->e);
		recorre = recorre.Siguiente();
	}
	return l;
}

// Sobrecarga del operador resta

TListaPoro TListaPoro::operator-(TListaPoro & lista) {
	TListaPoro l;
	TListaPosicion recorre;
	recorre = Primera();
	while(!recorre.EsVacia()) {
		if(!lista.Buscar(recorre.pos->e))
			l.Insertar(recorre.pos->e);
		recorre = recorre.Siguiente();
	}
	return l;

}

// Devuelve true si la lista está vacía, false en caso contrario.

bool TListaPoro::EsVacia()const  {
	if(this->primero == NULL && this->ultimo == NULL) { //NO PUEDO PONER LONGITUD()
		return true;
	}
	else {
		return false;
	}
}

// Inserta el elemento en la lista

bool TListaPoro::Insertar(TPoro & poro) {
	bool insertado = false;
	bool encontrado = false;
	TListaNodo * aux;

	if(this->EsVacia()) { //SI está vacía, insertamos el elemento sin mirar nada.
		aux = new TListaNodo();
		aux->e = poro;
		this->primero = aux;
		this->ultimo = aux;
		insertado = true;
	}
	else {
		if(!Buscar(poro)) {

			if(this->Primera().pos->e.Volumen() > poro.Volumen()) { // Insertar si va delante.
				aux = new TListaNodo();
				aux->e = poro;
				aux->siguiente = this->Primera().pos;
				this->Primera().pos->anterior = aux;
				this->primero = aux;
				insertado = true;
			}else if(this->Ultima().pos->e.Volumen() <= poro.Volumen()) {
				aux = new TListaNodo();
				aux->e = poro;
				aux->anterior = this->Ultima().pos;
				this->Ultima().pos->siguiente = aux;
				this->ultimo = aux;
				insertado = true;
			} else {
				TListaPosicion recorre, aux1;
				recorre  = this->Primera();

				while(!recorre.EsVacia() && !insertado) {
					aux1 = recorre;
					recorre = recorre.Siguiente();
					if(recorre.pos->e.Volumen() > poro.Volumen()) {
						aux = new TListaNodo();
						aux->e = poro;
						aux->anterior = aux1.pos;
						aux1.pos->siguiente = aux;
						recorre.pos->anterior = aux;
						aux->siguiente = recorre.pos;
						insertado = true;
					}

				}
			}
		}

	}
	return insertado;
}

// Busca y borra el elemento

bool TListaPoro::Borrar(TPoro & poro) { //COMPROBAR.
	bool borrado = false;
	TListaPosicion recorre,aux;

	if(Buscar(poro)) {
		recorre = this->Primera();
		while(!recorre.EsVacia() && !borrado) {

			if(poro == this->Primera().pos->e) {
				if(this->Longitud() != 1) {
					this->primero->siguiente->anterior = NULL;
					recorre.pos= recorre.pos->siguiente;
					delete this->primero;
					this->primero = recorre.pos;
				} else {
					this->~TListaPoro();
				}
				borrado = true;

			}else if (poro == this->Ultima().pos->e) {
				this->ultimo->anterior->siguiente = NULL;
				recorre.pos = this->ultimo->anterior;
				delete this->ultimo;
				this->ultimo = recorre.pos;

				borrado = true;
			} else {
				aux = recorre;
				recorre = recorre.Siguiente();
				if(recorre.pos->e == poro) {
					aux.pos->siguiente = recorre.pos->siguiente;
					recorre.pos->siguiente->anterior = aux.pos;
					delete recorre.pos;
					borrado = true;
				}
			}
		}
		return true;
	}
	return borrado;
}

// Borra el elemento que ocupa la posición indicada

bool TListaPoro::Borrar(TListaPosicion & pos) {
	return this->Borrar(pos.pos->e);
}

// Obtiene el elemento que ocupa la posición indicada

TPoro TListaPoro::Obtener(TListaPosicion & pos)const {
	TPoro poro;
	TListaPosicion aux;
	bool encontrado = false;

	aux = this->Primera();
	if(!aux.EsVacia()) {
		aux = Primera();
		while(!aux.EsVacia() && !encontrado) {
			//cout<<"94" <<endl;
			if(aux == pos) {
				//cout<<"ASIOS" <<endl;
				poro = aux.pos->e;
				encontrado = true;
			}else {
				//cout<<"ADIOS" <<endl;
				aux = aux.Siguiente();
			}
		}
	}
	//	delete aux; //CREO
	return poro;

}
/* POSIBLE MEJORA. HAY QUE COMPROBAR.
 * aux = this->Primera();
	if(!aux.EsVacia()) {
		while((aux.pos!=NULL)) {
			if(aux == pos) {
				return aux.pos.e;
			}
			aux.pos= aux.pos->siguiente;
		}
	}*/


// Devuelve true si el elemento está en la lista, false en caso contrario

bool TListaPoro::Buscar(TPoro & poro) const { //COMPROBAR.
	bool encontrado = false;
	if(this->EsVacia()) {
		encontrado = false;
	}
	else {
		TListaPosicion aux;
		aux = this->Primera();
		while(!aux.EsVacia() && !encontrado) { //Recorro la lista hasta que sea el final o haya encontrado la lista.
			if(aux.pos->e == poro) { //Si ya existe en la lista, pues se notifica.
				encontrado = true;
			}
			aux = aux.Siguiente(); //Avanzamos a la siguente posición de la lista. aux = aux.siguente*.
		}
	}

	return encontrado;
}

// Devuelve la longitud de la lista

int TListaPoro::Longitud() const{
	TListaPosicion aux;
	int longitud = 0;
	//   if(!this->EsVacia()) {
	aux = this->Primera();
	while(!aux.EsVacia()) { //Va contando hasta que
		longitud++;
		aux = aux.Siguiente();
	}
	// }
	return longitud;
}

// Devuelve la primera posición en la lista

TListaPosicion TListaPoro::Primera() const {
	TListaPosicion aux;
	if(!this->EsVacia()) {
		aux.pos = this->primero;
	}
	return aux;
}

// Devuelve la última posición en la lista

TListaPosicion TListaPoro::Ultima() const{
	TListaPosicion aux;
	if(!this->EsVacia()) {
		aux.pos = this->ultimo;
	}
	return aux;
}
//CREAR MÓDULO MOSTRAR. Que muestra todos los elementos de la lista.
// Extraer un rango de nodos de la lista

TListaPoro TListaPoro::ExtraerRango(int n1, int n2) {

}


