/* 
 * File:   tvectorporo.cpp
 * Author: Francisco Manuel García Botella, 48570019-E
 *
 * Created on 12 de febrero de 2013, 10:38
 */

#include "../include/tvectorporo.h"

 ostream & operator<<(ostream & salida, const TVectorPoro &vector) {
	salida<<"[";
	for(int i = 0; i < vector.Longitud();i++) {
                salida<<i+1 <<" " <<vector.datos[i];
		if(i < (vector.Longitud() -1)) {
			salida<<" ";
		}
	}
	salida<<"]";
        
	return salida;
	}

    TVectorPoro::TVectorPoro() {
        this->dimension = 0;
        this->datos = NULL;
    }

    TVectorPoro::TVectorPoro(int dimension) {
        if(dimension <= 0) {
            this->dimension = 0;
            this->datos = NULL;
        }
        else {
            this->dimension = dimension;
            this->datos = new TPoro[this->dimension]; //NO DEBO HACER NULL SINO ME CARGO LA POSICIÓN DE MEMORIA QUE ME DEVUELVE EL NEW.
        }
    }

    //Constructor copia.
    TVectorPoro::TVectorPoro(const TVectorPoro & vector) { 
        this->dimension = vector.Longitud();
        
        this->datos = new TPoro[this->dimension];
        for (int i = 0; i < this->dimension;i++) {
            this->datos[i] = vector.datos[i];
        }
    }
    
    //Destructor
    TVectorPoro::~TVectorPoro() {
        if(datos != NULL) {
                this->dimension = 0;
		delete [] datos;
		this->datos = NULL;
		}
    }
    
    //Sobrecarga del operador de asignación
    TVectorPoro & TVectorPoro::operator=(TVectorPoro & poro) {
        if((*this) != poro) {
            (*this).~TVectorPoro();
            this->dimension = poro.Longitud();
            this->datos = new TPoro[this->dimension];
            for (int i = 0; i < this->dimension; i++) {
                this->datos[i] = poro.datos[i];
            }
        }
        
        return (*this);
    }
    
    //METODOS
    
   //Sobrecarga del operador igualdad
    bool TVectorPoro::operator==(const TVectorPoro & poro) { //PROBAR
        bool igual = true;
        if(poro.Longitud() == this->Longitud()) {
            for (int i = 0; i < this->dimension && igual; i++) {
                if(this->datos[i] != poro.datos[i]) {
                    igual = false;
                }
            }
        } else {
            igual = false;
        }
        return igual;
    }
    

    
   // Devuelve la cantidad de posiciones ocupadas (no vacías) en el vector
    int TVectorPoro::Cantidad() { //FALTA COMPROBAR
        int ocupadas = 0;
        for (int i = 0; i < this->Longitud();i++) {
            if(this->datos != NULL){
                if(!this->datos[i].EsVacio()) 
                    ocupadas++;
            }
        }
        return ocupadas;
    }
   
    // Sobrecarga del operador corchete (parte IZQUIERDA)
    TPoro & TVectorPoro::operator[](int i) {
        if(i > 0 && i <= this->dimension)
	{
                return datos[i - 1];
	}
        else {
                return error;
	}
    }
    
    // Sobrecarga del operador corchete (parte DERECHA)
    TPoro TVectorPoro::operator[](int i) const {
        if(i > 0 && i <= this->dimension)
	{
            return datos[i - 1];
        }
	else {
            return error;
	}
    }
    
    // REDIMENSIONAR el vector de TPoro
    bool TVectorPoro::Redimensionar(int newtam) { 
        if(newtam > 0 && newtam != this->Longitud()) { //IF 1
             //Tam del vector supera al nuestro vector.
            TPoro *aux = new TPoro[newtam];

            if(newtam > this->Longitud()) {
                for(int i = 0; i < this->Longitud(); i++) {
                    aux[i] = this->datos[i];
                }
            }
            if(newtam < this->Longitud()) {
                  for(int i = 0; i < newtam; i++) {
                          aux[i] = this->datos[i];
                  }
            }

            this->dimension = newtam;
            delete [] this->datos;
            datos = NULL;
            datos = new TPoro[newtam];

            for(int i = 0; i < this->Longitud(); i++) {
                this->datos[i] = aux[i];
            }

            if(aux != NULL) {
                delete [] aux;
                aux = NULL;
            }
        return true;
    } //ENDIF 1
    else 
        return false;
    }
   







