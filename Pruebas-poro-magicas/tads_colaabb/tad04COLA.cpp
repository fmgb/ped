#include <iostream>
#include "TColaABBPoro.h"
#include "TECAP.h"
using namespace std;

int
main(void)
{
/******************************/
/***************** Longitud()*/
/*****************************/
  
  TColaABBPoro c1;
  TABBPoro a,b;
  TABBPoro *c;

  TPoro p10(1,2,10,"rojo");
  a.Insertar(p10);

  c1.Encolar(&a);
  c1.Encolar(&b);
  c1.Desencolar();

  c = c1.Cabeza();

  cout << "Longitud 1 : " << c1.Longitud() <<endl;

  c1.Desencolar();

  cout << "Longitud 2 : " << c1.Longitud() <<endl;

  return 1; 

}
