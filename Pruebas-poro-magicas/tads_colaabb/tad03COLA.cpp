#include <iostream>
#include "TColaABBPoro.h"
#include "TECAP.h"
using namespace std;

int
main(void)
{
/********************************/
/***************** ENCOLAR, "+" */
/********************************/
  
  TColaABBPoro c1,c2,c3;
  TABBPoro a,b,c,d;

  TPoro p10(1,2,10,"rojo");
  TPoro p20(1,2,20,"rojo");
  TPoro p30(1,2,30,"rojo");

  a.Insertar(p10);
  b.Insertar(p20);
  c.Insertar(p30);
  d.Insertar(p10);


  c1.Encolar(&a);
  c1.Encolar(&b);

  c3 = c1;

  c2.Encolar(&c);
  c2.Encolar(&d);

  cout << c3+c2 <<endl;

  return 1; 

}
