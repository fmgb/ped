#include <iostream>
#include "TColaABBPoro.h"
#include "TECAP.h"
using namespace std;

int
main(void)
{
/*********************************************/
/***************** ENCOLAR, DESENCOLAR, "<<" */
/*********************************************/
  
  TColaABBPoro c1;
  TABBPoro a;


  c1.Encolar(&a);
  c1.Desencolar();
  c1.Encolar(&a);
  c1.Desencolar();

  cout << c1 <<endl;

  return 1; 

}
