//PRUEBA 3. PRUEBA CON LISTA DE UN ELEMENTO Y PARAMETRO COLOR NULL
#include <iostream>

using namespace std;

#include "tlistaporo.h"

int
main(void)
{  TListaPoro a;
  TPoro  p(1, 1, 1.00, "rojo");
  a.Insertar(p);
  cout<<a.SubLista(0,0,NULL)<<endl;
  cout<<a.SubLista(1,1,NULL)<<endl;
  cout<<a.SubLista(2,2,NULL)<<endl;
  return 0;
}
