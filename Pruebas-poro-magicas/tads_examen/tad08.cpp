//PRUEBA 8. PRUEBA CON LISTA SIN NINGUNA COINCIDENCIA (SUBLISTAS VACIAS)
#include <iostream>

using namespace std;

#include "tlistaporo.h"

int
main(void)
{TListaPoro a;

TPoro  p;
for (int i=0; i<10; i++)
{ 
 a.Insertar(p);
 p.Posicion(i,i);
 p.Volumen(i+1);
 p.Color("verde");
}

cout<<a.SubLista(1,9,"verde")<<endl; //sublista 0 elementos
cout<<a.SubLista(0,0,NULL)<<endl; //Sublista 0 elementos
cout<<a.SubLista(10,20,"azul")<<endl; //Sublista 0 elementos
return 0;
}
