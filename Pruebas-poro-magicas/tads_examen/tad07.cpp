//PRUEBA 7. PRUEBA CON LISTA CON TODOS LOS COLORES A NULL
#include <iostream>

using namespace std;

#include "tlistaporo.h"

int
main(void)
{TListaPoro a;

TPoro  p;
for (int i=0; i<10; i++)
{ 
 a.Insertar(p);
 p.Posicion(i,i);
 p.Volumen(i+1);
 p.Color(NULL);
}

cout<<a.SubLista(3,7,"rojo")<<endl; //sublista 5 elementos
cout<<a.SubLista(-1,0,"rojo")<<endl; //sublista con 1 elemento vacío
cout<<a.SubLista(-2,-1,NULL)<<endl; //todos elementos
  return 0;
}
