//PRUEBA 4. PRUEBA CON LISTA DE UN ELEMENTO TPORO VACIO
#include <iostream>

using namespace std;

#include "tlistaporo.h"

int
main(void)
{  TListaPoro a;
  TPoro  p;
  a.Insertar(p);
  cout<<a.SubLista(0,0,NULL)<<endl;
  cout<<a.SubLista(0,0,"verde")<<endl;
  cout<<a.SubLista(2,2,NULL)<<endl;
  return 0;
}
