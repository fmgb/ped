//PRUEBA 6. PRUEBA CON LISTA RANGOS V1 > V2
#include <iostream>

using namespace std;

#include "tlistaporo.h"

int
main(void)
{TListaPoro a;

TPoro  p;
for (int i=0; i<10; i++)
{ 
 
 p.Posicion(i,i);
 p.Volumen(i+1);
 p.Color("verde");
 a.Insertar(p);
}

cout<<a.SubLista(7,3,"rojo")<<endl; //sublista 0 elementos
cout<<a.SubLista(7,3,"verde")<<endl; //Toda la lista
  return 0;
}
