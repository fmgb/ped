//PRUEBA 10. PRUEBA CON LISTA 1000 ELEMENTOS 
#include <iostream>

using namespace std;

#include "tlistaporo.h"

int
main(void)
{TListaPoro a;

TPoro  p;
for (int i=0; i<1000; i++)
{  
 p.Posicion(i,i);
 p.Volumen(i+1);
 if ((i+1)%2 == 0) p.Color("verde");
 else p.Color("azul");
 a.Insertar(p);
}

cout<<a.SubLista(1,1000,"azul")<<endl; //sublista 500 elementos
cout<<a.SubLista(1,1000,"verde")<<endl; //Sublista 500 elementos
cout<<a.SubLista(1,1000,"rojo")<<endl; //Sublista 1000 elementos
cout<<a.SubLista(1001,2000,"rojo")<<endl; //Sublista 0 elementos
return 0;
}
