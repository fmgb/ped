//PRUEBA 2. PRUEBA CON LISTA DE UN ELEMENTO.
#include <iostream>

using namespace std;

#include "tlistaporo.h"

int
main(void)
{  TListaPoro a;
  TPoro  p(1, 1, 1.00, "rojo");
  a.Insertar(p);
  cout<<a.SubLista(0,0,"rojo")<<endl;
  cout<<a.SubLista(1,1,"rojo")<<endl;
  cout<<a.SubLista(1,1,"azul")<<endl;
  return 0;
}
