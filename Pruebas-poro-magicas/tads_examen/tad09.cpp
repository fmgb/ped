//PRUEBA 9. PRUEBA CON LISTA CON TODOS NODOS COINCIDENTES 
#include <iostream>

using namespace std;

#include "tlistaporo.h"

int
main(void)
{TListaPoro a;

TPoro  p;
for (int i=0; i<10; i++)
{  
 p.Posicion(i,i);
 p.Volumen(i+1);
 p.Color("verde");
 a.Insertar(p);
}

cout<<a.SubLista(1,10,"azul")<<endl; //sublista 10 elementos
cout<<a.SubLista(11,20,"verde")<<endl; //Sublista 10 elementos
return 0;
}
