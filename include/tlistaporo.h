/* 
 * File:   tlistaporo.h
 * Author: Francisco Manuel García Botella, 48570019-E
 *
 * Created on 10 de marzo de 2013, 20:34
 */

#ifndef TLISTAPORO_H
#define	TLISTAPORO_H
#include <iostream>
#include <iomanip>
#include "../include/tporo.h"
#include <string.h>


using namespace std;

class TListaNodo {
    friend class TListaPoro;
    friend class TListaPosicion;
private:
    //El elemento del nodo
    TPoro e;
    
    //El nodo anterior
    TListaNodo *anterior;
    
    //El nodo siguiente
    TListaNodo *siguiente;
    
public:
    //Constructor por defecto
    TListaNodo();
    
    //Constructor de copia
    TListaNodo(const TListaNodo &);
    
    //Destructor
    ~TListaNodo();
    
    //Sobrecarga del operador asignación
    TListaNodo & operator=( TListaNodo &);
};

class TListaPosicion {
    friend class TListaPoro;
private:
    TListaNodo *pos;
    
public:
    //Constructor por defecto
    TListaPosicion();
    
    //Constructor de copia
    TListaPosicion (const TListaPosicion &);
    
    //Destructor
    ~TListaPosicion();
    
    //Sobrecarga del operador asignación
    TListaPosicion & operator=(const TListaPosicion &);
    
    //Sobrecarga del operador igualdad
    bool operator==( const TListaPosicion & );
    
    //Devuelve la posicion anterior
    TListaPosicion Anterior();
    
    //Devuelve la posicion siguiente.
    TListaPosicion  Siguiente();
    
    //Devuelve TRUE si la posición no apunta a una lista, FALSE en caso contario.
    bool EsVacia();
};

class TListaPoro {
    
    //FUNCIONES AMIGAS
    //Sobrecarga del operador salida
    friend ostream & operator<<(ostream & salida, const TListaPoro & lista);


private:
    // Primer elemento de la lista
    TListaNodo *primero;
    // Ultimo elemento de la lista
    TListaNodo *ultimo;
    //Indica el número de elementos que contiene esa lista.
    int numelem;
public:
    
    //FORMA CANÓNICA
    
    // Constructor por defecto
    TListaPoro();
    
    // Constructor de copia
    TListaPoro (const TListaPoro &);
    
    // Destructor
    ~TListaPoro ();
    
    // Sobrecarga del operador asignación
    TListaPoro & operator=(const TListaPoro &);
    
    //MÉTODOS
    
    // Sobrecarga del operador igualdad
    bool operator==(TListaPoro &);
    
    // Sobrecarga del operador suma
    TListaPoro operator+(TListaPoro &);
    
    /**
     * Sobrecarga del operador -
     * @param 
     * @return Lista Nueva. Contiene los elementos de la primera lista(IZQUIERDA) que NO existen en la segunda lista(DERECHA).
     */
    TListaPoro operator-(TListaPoro & segunda);
    
    /**
     * Metodo publico.
     * @return TRUE si la lista está vacía, FALSE en caso contrario
     */
    bool EsVacia() const ;
    
    // Inserta el elemento en la lista
    bool Insertar(TPoro &);
    
    // Busca y borra el elemento
    bool Borrar(TPoro &);
    
    // Borra el elemento que ocupa la posición indicada
    bool Borrar(TListaPosicion &);
    
    // Obtiene el elemento que ocupa la posición indicada
    TPoro Obtener(TListaPosicion &)const;
    
    // Devuelve true si el elemento está en la lista, false en caso contrario
    bool Buscar(TPoro &)const;
    
    // Devuelve la longitud de la lista
    int Longitud()const ;
    
    // Devuelve la primera posición en la lista
    TListaPosicion Primera()const ;
    
    // Devuelve la última posición en la lista
    TListaPosicion Ultima()const;
    
    // Extraer un rango de nodos de la lista
    TListaPoro ExtraerRango (int n1, int n2);
};


#endif	/* TLISTAPORO_H */

