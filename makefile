CC = g++
FLAGS = -g -Wall -Wextra -pedantic
OBJECTS= tporo.o tvectorporo.o tlistaporo.o tad.o

all: tad clean

tad: $(OBJECTS)
	$(CC) $(FLAGS) $(OBJECTS) -o tad

tad.o: src/tad.cpp include/tporo.h
	$(CC) $(FLAGS) -c src/tad.cpp


tporo.o: lib/tporo.cpp include/tporo.h
	$(CC) $(CFLAGS) -c lib/tporo.cpp

tvectorporo.o: lib/tvectorporo.cpp include/tvectorporo.h include/tporo.h
	$(CC) $(CFLAGS) -c lib/tvectorporo.cpp

tlistaporo.o: lib/tlistaporo.cpp include/tlistaporo.h include/tporo.h
	$(CC) $(CFLAGS) -c lib/tlistaporo.cpp


clean:
	rm $(OBJECTS)
